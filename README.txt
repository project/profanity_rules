
-- SUMMARY --

The Profanity Rules module provide an interface to create different set of 
rules for profanity and use these rules accordingly for fields in the content 
types. This module also provide an admin setting form to list all the abusive 
word which will be restricted during form submit and error messages accordingly 
for specific rules.


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.
* Download and unpack the Global Profanity module directory in your modules
   folder (this will usually be "sites/all/modules/").
* Go to "Administer" -> "Modules" and enable the module.

-- CONFIGURATION --

* To use this module goto Administration >> Configuration >> Profanity Rules.
* There you will get Add Profanity Rules link. A form will be open, 
  when you click on the link.
* The form will contain rule name, word list, custom error message
  and a checkbox to activate the rule.
* In the word list enter each words in newline.
