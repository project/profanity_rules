<?php

/**
 * @file
 * Include file for profanity rules module.
 *
 * Contains functionality related to admin forms.
 */

/**
 * Form callback for adding profanity rules.
 */
function profanity_rules_add_form($form, &$fom_state, $machine_name = NULL) {
  $locked = $machine_name != NULL ? 1 : 0;
  $profanity_type = profanity_rules_type_load($machine_name);
  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => isset($profanity_type['name']) ? $profanity_type['name'] : '',
    '#description' => t('The human readable name of this profanity type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => $machine_name != NULL ? $machine_name : '',
    '#maxlength' => 32,
    '#disabled' => $locked,
    '#machine_name' => array(
      'exists' => 'node_type_load',
    ),
    '#description' => t('The human readable name of this profanity type.'),
  );
  $form['word_list'] = array(
    '#title' => t('Word List'),
    '#type' => 'textarea',
    '#default_value' => isset($profanity_type['word_list']) ? $profanity_type['word_list'] : '',
    '#required' => TRUE,
    '#rows' => 5,
    '#size' => 30,
    '#description' => t('Put all words need to be block, separated by newline'),
  );
  $form['profanity_type'] = array(
    '#type' => 'value',
    '#value' => $profanity_type,
  );
  $form['error_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom error message'),
    '#description' => t("Specify an error message that should be displayed when user input doesn't pass validation"),
    '#required' => TRUE,
    '#size' => 60,
    '#maxlength' => 255,
    '#default_value' => isset($profanity_type['error_message']) ? $profanity_type['error_message'] : '',
  );
  $form['status'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
    '#default_value' => isset($profanity_type['status']) ? $profanity_type['status'] : 0,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save profanity rule'),
    '#weight' => 40,
  );
  $form['actions']['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Cancel'), 'admin/config/system/profanity-rules'),
    '#weight' => 41,
  );
  if ($machine_name != NULL) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete profanity type'),
      '#weight' => 45,
    );
  }
  return $form;
}

/**
 * Form submission handler for profanity_rules_add_form().
 *
 * @see profanity_rules_add_form_validate()
 */
function profanity_rules_add_form_submit($form, &$form_state) {
  $data = array(
    'id' => isset($form_state['values']['profanity_type']['id']) ? $form_state['values']['profanity_type']['id'] : '',
    'machine_name' => $form_state['values']['type'],
    'name' => check_plain(trim($form_state['values']['name'])),
    'word_list' => check_plain($form_state['values']['word_list']),
    'status' => $form_state['values']['status'],
    'error_message' => check_plain($form_state['values']['error_message']),
  );
  if (empty($form_state['values']['profanity_type'])) {
    $data = drupal_write_record('profanity_rules', $data);
    drupal_set_message(t('profanity rules has been created.'), 'status');
  }
  else {
    $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
    if ($op == t('Delete profanity rules')) {
      $form_state['redirect'] = 'profanity-rules/' . $form_state['values']['type'] . '/delete';
      return;
    }
    $data = drupal_write_record('profanity_rules', $data, 'id');
    drupal_set_message(t('Profanity rules type has been updated.'), 'status');
  }
  $form_state['redirect'] = 'admin/config/system/profanity-rules';
}

/**
 * Callback function for listing of profanity rules.
 */
function profanity_rules_list() {
  $build = array();
  $header = array(
    array('data' => t('ID'), 'field' => 'id', 'sort' => 'asc'),
    array('data' => t('Name'), 'field' => 'name', 'sort' => 'asc'),
    array('data' => t('Status'), 'field' => 'status', 'sort' => 'asc'),
    array('data' => t('Operations'), 'colspan' => 2),
  );
  $profanity_rules_types = db_select('profanity_rules')
          ->fields('profanity_rules')
          ->extend('PagerDefault')
          ->extend('TableSort')
          ->orderByHeader($header)
          ->limit(10)
          ->execute()->fetchAll();
  $build['add_link'] = array(
    '#markup' => l(t('Add Profanity Rules'), 'add/profanity-rules'),
  );
  if (!empty($profanity_rules_types)) {
    $rows = array();

    foreach ($profanity_rules_types as $type) {
      $rows[] = array(
        check_plain($type->id),
        $type->name,
        $type->status,
        l(t('Edit'), 'profanity-rules/' . $type->machine_name . '/edit'),
        l(t('Delete'), 'profanity-rules/' . $type->machine_name . '/delete'),
      );
    }
    $build['table'] = array(
      '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
    );
    $build['pager'] = array(
      '#markup' => theme('pager'),
    );
  }
  else {
    $build['no_result'] = array(
      '#markup' => '<div>' . t('No results found.') . '</div>',
    );
  }
  return $build;
}

/**
 * Callback function for deleting single profanity rules.
 */
function profanity_rules_delete_confirm($form, &$form_state, $machine_name) {
  $form['type'] = array('#type' => 'value', '#value' => $machine_name);
  $message = t('Are you sure you want to delete the profanity rule %type?', array('%type' => $machine_name));
  $caption = t('This action cannot be undone.');
  return confirm_form($form, $message, 'admin/config/system/profanity-rules', $caption, t('Delete'));
}

/**
 * Process profanity rules type delete confirm submissions.
 *
 * @see profanity_rules_delete_confirm()
 */
function profanity_rules_delete_confirm_submit($form, &$form_state) {
  db_delete('profanity_rules')
      ->condition('machine_name', $form_state['values']['type'])
      ->execute();
  db_delete('profanity_rules_fields')
      ->condition('profanity_machine_name', $form_state['values']['type'])
      ->execute();
  $form_state['redirect'] = 'admin/config/system/profanity-rules  ';
}
